let user_score = document.getElementById("user-score");
let computer_score = document.getElementById("computer-score");
let announcement = document.getElementById("announcement");
let rock_button = document.getElementById("rock");
let paper_button = document.getElementById("paper");
let scissors_button = document.getElementById("scissors");

let userPoints = 0;
let computerPoints = 0;

function getComputerSelection() {

    let avaiableChoice = ["r", "s", "p"];
    computerSelection = avaiableChoice[Math.floor(Math.random() * avaiableChoice.length)];
    return computerSelection;
}


function main() {

    rock_button.addEventListener("click", function() {
        game("r")
    });

    paper_button.addEventListener("click", function() {
        game("p")
    })

    scissors_button.addEventListener("click", function() {
        game("s");
    })
}
main();


function game(userSelection) {

    let computerFinalResult = getComputerSelection();
    let comparisonString = userSelection + computerFinalResult;
    compare(comparisonString);

}


function compare(someString) {

    switch (someString) {
        case "pr":
        case "rs":
        case "sp":
            win();
            break;


        case "rp":
        case "ps":
        case "sr":
            loose();
            break;

        case "pp":
        case "rr":
        case "ss":
            draw();
            break;
    }

}

function win() {

    announcement.innerHTML = "User WINS! ";
    userPoints++;
    user_score.innerHTML = userPoints;
    computer_score.innerHTML = computerPoints;


}

function loose() {

    announcement.innerHTML = "User LOSE";
    computerPoints++;
    computer_score.innerHTML = computerPoints;
}

function draw() {

    announcement.innerHTML = "DRAW";

}


function scoreCounter(score) {

    let newScore = ++score;
    score.innerHTML = "newScore";
    console.log(newScore);
    return newScore;

}